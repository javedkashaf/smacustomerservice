package com.ps.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cust_tab")
public class Customer {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE) //for oracle
	@GeneratedValue(strategy = GenerationType.IDENTITY) //for mysql
	@Column(name="cust_id_col")
	private Long id;
	
	@Column(name="cust_name_col")
	private String name;
	
	@Column(name="cust_email_col")
	private String email;
	
	@Column(name="cust_gender_col")
	private String gender;
	
	@Column(name="cust_img_col")
	private String imagePath;
	
	@Column(name="cust_mob_col")
	private String mobile;
	
	@Column(name="cust_addr_col")
	private String address;
	
	@Column(name="cust_pan_col")
	private String panCardId;
	
	@Column(name="cust_aadhar_col")
	private String aadharId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPanCardId() {
		return panCardId;
	}

	public void setPanCardId(String panCardId) {
		this.panCardId = panCardId;
	}

	public String getAadharId() {
		return aadharId;
	}

	public void setAadharId(String aadharId) {
		this.aadharId = aadharId;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", gender=" + gender + ", imagePath="
				+ imagePath + ", mobile=" + mobile + ", address=" + address + ", panCardId=" + panCardId + ", aadharId="
				+ aadharId + "]";
	}

	public Customer() {
		super();
	}
	
	
	
}
	
	
	
	

